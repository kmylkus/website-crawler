# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urljoin

class E27Spider(scrapy.Spider):
    name = 'e27'
   # allowed_domains = ['example.com']
    start_urls = ['https://e27.co/startups/']

    def parse(self, response):
        
       for post_link in response.xpath('//div[@class = "startup-block startup-list-item"]/a/@href').extract():
            url = urljoin(response.url, post_link)
            print(url)
#response.xpath('//div[@id="images"]/a').getall()

from selenium import webdriver
import time
driver = webdriver.Firefox(executable_path='D:\selenium-driver\geckodriver.exe')
driver.get('https://e27.co/startups/')



while True:
    try:
        loadMoreButton = driver.find_element_by_xpath("//button[contains(.,'Load More')]")
        time.sleep(2)
        loadMoreButton.click()
        time.sleep(2)
    except Exception as e:
        print(e)
        break
print("Complete")
items = driver.find_elements_by_class_name("startuplink") 
a = []
for item in items:
    href = item.get_attribute('href')
    a.append(href)